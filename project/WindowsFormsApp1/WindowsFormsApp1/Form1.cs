﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public int score = 0;
        public Form1()
        {
            InitializeComponent();
            panel2.Enabled = false;
            panel2.Visible = false;
            panel3.Enabled = false;
            panel3.Visible = false;
            panel4.Enabled = false;
            panel4.Visible = false;
            panel5.Enabled = false;
            panel5.Visible = false;
            panel6.Enabled = false;
            panel6.Visible = false;
            panel7.Enabled = false;
            panel7.Visible = false;
           
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //StreamWriter SW = new StreamWriter("C:\\Users\\Max\\Desktop\\project\\WindowsFormsApp1\\WindowsFormsApp1\\TextFile1.txt");
            //SW.WriteLine(textBox1.Text);
            //SW.Close();
            panel1.Enabled= false;
            panel1.Visible=false;
            panel2.Enabled = true;
            panel2.Visible = true;
        }

        //private void radioButton2_CheckedChanged(object sender, EventArgs e)
        //{

        //}

        private void button2_Click(object sender, EventArgs e)
        {
            if (radioButton1.Checked)
            {
                score += Convert.ToInt32(radioButton1.Tag); ;
            }
            if (radioButton2.Checked)
            {
                score += Convert.ToInt32(radioButton2.Tag);
            }
            panel2.Enabled = false;
            panel2.Visible = false;
            panel3.Enabled = true;
            panel3.Visible = true;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (radioButton3.Checked)
            {
                score += Convert.ToInt32(radioButton3.Tag);
            }
            if (radioButton4.Checked)
            {
                score += Convert.ToInt32(radioButton4.Tag);
            }
            panel3.Enabled = false;
            panel3.Visible = false;
            panel4.Enabled = true;
            panel4.Visible = true;
            
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (radioButton5.Checked)
            {
                score += Convert.ToInt32(radioButton5.Tag);
            }
            if (radioButton6.Checked)
            {
                score += Convert.ToInt32(radioButton6.Tag);
            }
            panel4.Enabled = false;
            panel4.Visible = false;
            panel5.Enabled = true;
            panel5.Visible = true;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            if (radioButton7.Checked)
            {
                score += Convert.ToInt32(radioButton7.Tag);
            }
            if (radioButton8.Checked)
            {
                score += Convert.ToInt32(radioButton8.Tag);
            }
            panel5.Enabled = false;
            panel5.Visible = false;
            panel6.Enabled = true;
            panel6.Visible = true;
        }

        private void button6_Click(object sender, EventArgs e)
        {
            if (radioButton9.Checked)
            {
                score += Convert.ToInt32(radioButton9.Tag);
            }
            if (radioButton10.Checked)
            {
                score += Convert.ToInt32(radioButton10.Tag);
            }
            panel6.Enabled = false;
            panel6.Visible = false;
            panel7.Enabled = true;
            panel7.Visible = true;
           
            label7.Text = string.Format("правильних відповідей {0} з 5", score);

        }

        private void button7_Click(object sender, EventArgs e)
        {
            StreamWriter SW = new StreamWriter("C:\\Users\\Max\\Desktop\\project\\WindowsFormsApp1\\WindowsFormsApp1\\TextFile1.txt");
            SW.WriteLine(string.Format("{0},правильних відповідей {1} з 5", textBox1.Text,score));
            SW.Close();
            
            this.Close();
        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void saveFileDialog1_FileOk(object sender, CancelEventArgs e)
        {
            
        }

        //private void button8_Click(object sender, EventArgs e)
        //{
        //    if (saveFileDialog1.ShowDialog() == DialogResult.OK)
        //    {
        //        StreamWriter SW = new StreamWriter("textfile.txt");
        //        SW.WriteLine(string.Format("{0},правильних відповідей {1} з 5", textBox1.Text, score));
        //        SW.Close();
        //    }
        //}
    }
}
